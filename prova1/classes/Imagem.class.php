<?php

/**
 *Esta classe vai fazer o upload de imagens
 * @author Keisy Barbosa
 */
class Imagem
{
    /**
     *Função para verificar se o arquivo é imagem
     *@param array $img vai pegar extensão
     *@return boolean
     */
    function verificarImg($img)
    {//faz o tratamento da imagem
        if ($img) {
            //verificar se o arquivo é imagem 
            $ext = "";
            switch ($img) { //verifica a extensão
                case "image/png":
                    $ext = true;
                    break;
                case "image/jpeg":
                case "image/jpg":
                    $ext = true;
                    break;
                default:
                    $ext = false;
            }

            return $ext;
        } else {
            return false;
        }
    }
    /**
     *Função para enviar imagem
     *@param string $nomeUser pega o nome do usuário
     *@param string $nomeArqu recebe o nome do arquivo
     *@param string $men armazena a descrição do usuário
     *@param array $img recebe a imagem 
     *@return boolean
     */

    function criaImgAndMen($nomeUser, $nomeArqu, $men, $img)
    {
        if(!file_exists("img/men")){//cria a pasta caso ela não exista
            mkdir("img/men", 0777, true);
        }
        if (!file_exists("img/$nomeArqu")) { //verifica se a imagem já existe
            move_uploaded_file($img['tmp_name'], "img/$nomeArqu");//envia a imagem como se fose a primeira
            if ($men) { //gera um indice que armazena a descrição do usuario
                $ext = substr($img['type'],6);
                $arq = fopen("img/men/$nomeUser$ext.txt", "a");
                fwrite($arq, $men);
                fclose($arq);
            }
        } else {
            $n = 0;
            $novo = "";
            while (file_exists("img/$nomeArqu")) {
                $novo = "{$n}$nomeArqu";
                if (file_exists("img/$novo")) {
                    $n++;
                } else {
                    break;
                }
            }
            move_uploaded_file($img['tmp_name'], "img/$novo"); //envia a primeira imagem
            if ($men) { //gera um indice que armazena a descrição do usuario
                $ext = substr($img['type'],6);
                $arq = fopen("img/men/{$n}$nomeUser$ext.txt", "a");
                fwrite($arq, $men);
                fclose($arq);
            }
        }
        return true;
    }
    /**
     *Função para ler todos os arquivos
     *@param string $img pega o ponteiro do diretorio
     *@return void
     */
    function slide($img)
    {
        $mensagens = array();
        $mens = opendir("img/men");
        while(false != ($men = readdir($mens))){
            if($men == "." || $men == ".."){
                continue;
            }else{
                $nome = explode(".", $men); //pega o nome da menssagem 
                $arq = fopen("img/men/$men","r");
                $mensagens[$nome[0]]['men'] = fread($arq, file_exists("img/men/$men"));
            }
        }
        while (false != ($ler = readdir($img))) { //ler todos os arquivos
            if ($ler == "." || $ler == ".." || $ler == "men") { //verifica se é o diretório atual ou o direório anterior
                continue;
            } else { //gera os slides
                $nome = explode(".", $ler); //pega o nome da imagem           
                echo "
            <li>
                <img src=\"img/$ler\">";
                if (isset($mensagens[$nome[0]]["men"])) { //verifica se existe legenda sobre a imagem
                    echo "
                    <div class=\"uk-overlay uk-overlay-primary uk-position-bottom uk-text-center uk-animation-slide-bottom\">
                        <p class=\"uk-margin-remove\">{$mensagens[$nome[0]]['men']}</p>
                    </div>
                ";
                }
                echo "
            </li>";
            }
        }
    }
}
?>

<?php
require_once("classes/Imagem.class.php");
$nomeUser = ($_POST['nome'] != "") ? $_POST['nome'] : "sem_nome";//pega o nome do usuario 
$img = (isset($_FILES['foto'])) ? $_FILES['foto'] : null; //pega imagem
$men = (isset($_POST['men'])) ? addslashes($_POST['men']) : null; // pega mensagem
$imagem = new Imagem();
if (strlen($men) > 90) { //verifica se o usuario excedeu o limite de caracteres
    session_start(); //inicia a sessao
    $_SESSION['erro'] = "Coloque uma legenda com no minimo 90 caracteres!";
    header("Location: index.php");
}
//enviar a imagem
if ($imagem->verificarImg($img["type"])) {
    $ext = substr($img['type'], 6); //pega a extensão da imagem
    $nomeArqu = "$nomeUser$ext.$ext"; //cria o nome do arquivo
    if ($imagem->criaImgAndMen($nomeUser, $nomeArqu, $men, $img)) {
        require_once("html/cabecalho.html"); //gera o html
        echo "
                <div class=\"uk-alert alert uk-align-center\" uk-alert>
                    <p>Imagem enviada com sucesso...</p> <br />
                    Veja sua foto em <a href=\"slide.php\" class=\"uk-link-reset uk-button-text\" id=\"link1\">slider</a><br /> 
                    Adicionar nova <a href=\"index.php\" class=\"uk-link-reset uk-button-text\" id=\"link1\">imagem</a>
                </div>";
        require_once("html/rodape.html"); //gera o html
    } else {
        session_start(); //inicia a sessao
        $_SESSION['erro'] = "Erro ao enviar a imagem";
        header("Location: index.php");
    }
} else {
    session_start(); //inicia a sessao
    $_SESSION['erro'] = "Envie uma imagem valida (jpeg, png, jpg)";
    header("Location: index.php");
}
unset($img); //deleta a variavel que pega o files
unset($nomeUser); //deleta a variavel que pega o nome do usuario
unset($men); //deleta a variavel que pega o comentário
unset($_FILES); //apaga a variavel files
unset($_POST); //apaga a variavel post
?>
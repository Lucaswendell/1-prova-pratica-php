<?php
session_start();
if(file_exists("img")){
    require_once("classes/Imagem.class.php");
    $img = opendir("img"); //coloca o diretorio img em um ponteiro
}else{
    mkdir("img",0777, true);
    $img = opendir("img"); //coloca o diretorio img em um ponteiro
}
echo "
<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\" />
    <link rel=\"stylesheet\" href=\"css/uikit.min.css\" />
    <link rel=\"stylesheet\" href=\"css/meuCss.css\" />
    <title>Slider</title>
</head>
<body>
    <div class=\"uk-navbar-container\" uk-navbar>
    <div class=\"uk-navbar-center\">
        <ul class=\"uk-navbar-nav\">
            <li><a href=\"index.php\"><span style=\"margin-right: 5px;\" class=\"uk-icon\" uk-icon=\"icon: image; ratio: 1.5;\"></span>Adicionar imagem.</a><li/>
        </ul>
    </div>
    </div>
    <div class=\"slie uk-position-center\">
    <div class=\"hv\" uk-slideshow=\"autoplay:true; animation:scale;autoplay-interval: 2000;\" id=\"slide\">
        <ul class=\"uk-slideshow-items\">
";
$gera = new Imagem();
$gera->slide($img);
echo "
        </ul>
        <a href=\"#\" uk-slideshow-item=\"previous\" class=\"uk-position-center-left-out uk-position-small\" uk-slidenav-previous></a>
        <a href=\"#\" uk-slideshow-item=\"next\" uk-slidenav-next class=\"uk-position-center-right-out uk-position-small\"></a>
        <ul class=\"uk-slideshow-nav uk-dotnav uk-flex-center uk-margin\"></ul>
    </div>
    </div>
</body>
<script src=\"js/uikit.min.js\"></script>
<script src=\"js/uikit-icons.min.js\"></script>
</html>";
closedir($img);
?>
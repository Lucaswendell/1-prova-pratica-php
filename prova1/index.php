<!DOCTYPE html>
<html lang="ph-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/uikit.min.css" />
    <link rel="stylesheet" href="css/meuCss.css">
    <title>Envie sua imagem</title>
</head>
<body>
    <div id="form">
        <p id="formP" class="uk-text-center">Envie uma imagem e veja ela passar em um slide</p>
        <form action="recebe.php" method="post" enctype="multipart/form-data">
            <div class="uk-inline uk-form-width-large">
                <span class="uk-form-icon" uk-icon="icon: user; ratio: 1.5;"></span>
                <input class="uk-input" type="nome" name="nome" placeholder="Digite seu nome(opcional)" />
            </div><br /><br />
            <textarea placeholder="Digite uma legenda para a imagem(opcional)" class="uk-textarea" name="men" id="legenda" cols="20" rows="10" onblur="limite(this.value)"></textarea><br /><br />
            <div class="uk-placeholder uk-text-center">
                <p id="imgSelect"><span class="uk-icon" uk-icon="icon:image; ratio:1.5;"></span> Nenhuma imagem selecionada</p>  
                <div uk-form-custom>
                <span uk-icon="icon: cloud-upload"></span>
                    <input type="file" name="foto" onchange="aparece(this.value)" id="file" accept="image/jpeg,image/png,image/jpg"/>
                    <span class="uk-link">Selecione a imagem</span>
                </div>
            </div>
            <br /><br />
            <input type="submit" value="enviar"  class="uk-button uk-button-primary uk-align-center">
        </form>
    </div>
    <script src="js/uikit.min.js"></script>
    <script src="js/uikit-icons.min.js"></script>
    <script src="js/meuJs.js"></script>
    <?php 
    session_start();
    if (isset($_SESSION['erro'])) {
        $erro = $_SESSION['erro'];
        echo "
<script>
    UIkit.notification({
        message: \"{$erro}\",
        status: \"danger\"
    });
</script>";
        session_destroy();
    }
    ?>
</body>

</html>

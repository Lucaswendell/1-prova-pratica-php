//verifica se os campos nao estao preenchidos
function validarLogin() {
    var user = document.getElementById("user").value;
    var pass = document.getElementById("pass").value;
    var erro = 0;
    if (user == "") {
        erro++;
    }
    if (pass == "") {
        erro++;
    }
    if (erro > 0) {
        document.getElementById("erro").classList = "uk-alert uk-alert-danger";
        document.getElementById("erro").innerHTML = "Preencha todos os campos!!!";
        return false;
    } else {
        return true;
    }
}
function validar() {
    var user = document.getElementById("user").value;
    var pass = document.getElementById("pass").value;
    var repass = document.getElementById("repass").value;
    var erro = 0;
    if (user == "") {
        erro++;
    }
    if (pass == "" || repass == "") {
        erro++;
    }
    if (erro > 0) {
        document.getElementById("erro").classList = "uk-alert uk-alert-danger";
        document.getElementById("erro").innerHTML = "Preencha todos os campos!!!";
        return false;
    } else {
        return true;
    }
}
function validarNova() {
    var pass = document.getElementById("pass").value;
    var repass = document.getElementById("repass").value;
    var erro = 0;
    if (pass == "" || repass == "") {
        erro++;
    }
    if (erro > 0) {
        document.getElementById("erro").classList = "uk-alert uk-alert-danger";
        document.getElementById("erro").innerHTML = "Preencha todos os campos!!!";
        return false;
    } else {
        return true;
    }
}

//verifica se a senha um é igual a senha 2
function senha(senha1) {
    var pass = document.getElementById("pass").value;
    if (pass != senha1) {
        document.getElementById("erro").classList = "uk-alert uk-alert-danger";
        document.getElementById("erro").innerHTML = "Senhas diferentes";
        return false;
    } else {
        document.getElementById("erro").classList = "";
        document.getElementById("erro").innerHTML = "";
        return true;
    }
}
//olha a senha
document.getElementById("passO").onclick = function () {
    if (document.getElementById("pass").type == "password") {
        document.getElementById("pass").type = "text";
        document.getElementById("passO").setAttribute("uk-icon", "icon: unlock; ratio: 1.5;");
    } else {
        document.getElementById("pass").type = "password";
        document.getElementById("passO").setAttribute("uk-icon", "icon: lock; ratio: 1.5;");
    }
};
document.getElementById("passO2").onclick = function () {
    if (document.getElementById("repass").type == "password") {
        document.getElementById("repass").type = "text";
        document.getElementById("passO2").setAttribute("uk-icon", "icon: unlock; ratio: 1.5;");
    } else {
        document.getElementById("repass").type = "password";
        document.getElementById("passO2").setAttribute("uk-icon", "icon: lock; ratio: 1.5;");
    }
};
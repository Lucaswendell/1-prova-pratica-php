<?php
require_once("class/Recebe.class.php");
$proce = new Recebe();
if ($_GET['acao'] == "cadastro" && isset($_GET['acao'])) {
    $nome = isset($_POST['nome']) ? addslashes($_POST['nome']) : null;
    $senha = isset($_POST['pass']) ? addslashes($_POST['pass']) : null;
    $senha2 = isset($_POST['repass']) ? addslashes($_POST['repass']) : null;
    if ($senha != $senha2) {
        session_start();
        $_SESSION['erro'] = "Senhas diferentes.";
        header("Location: cadastro.php");
        exit();
    }
    if ($senha == "" || $senha2 == "") {
        session_start();
        $_SESSION['erro'] = "Preencha todos os campos.";
        header("Location: cadastro.php");
        exit();
    }
    if ($nome != "") {
        if ($proce->cadastro($nome, $senha)) {
            session_start();
            $_SESSION['semErro'] = "Usuário cadastrado com sucesso...";
            header("Location: index.php");
            exit();
        } else {
            session_start();
            $_SESSION['erro'] = "Usuário já cadastrado";
            header("Location: cadastro.php");
            exit();
        }
    }
} else if ($_GET['acao'] == "logar" && isset($_GET['acao'])) {
    $nome = isset($_POST['nome']) ? addslashes($_POST['nome']) : null;
    $senha = isset($_POST['senha']) ? addslashes($_POST['senha']) : null;
    if ($nome == "") {
        session_start();
        $_SESSION['erro'] = "Preencha todos os campos!";
        header("Location: index.php");
        exit();
    }
    if ($senha == "") {
        session_start();
        $_SESSION['erro'] = "Preencha todos os campos!";
        header("Location: index.php");
        exit();
    }
    if (file_exists("user/$nome")) {
        if ($proce->login($nome, $senha)) {
            header("Location: index.php");
        } else {
            session_start();
            $_SESSION['erro'] = "Senha incorreta!";
            header("Location: index.php");
            exit();
        }
    } else {
        session_start();
        $_SESSION['erro'] = "Usuário não cadastrado";
        header("Location: index.php");
        exit();
    }
} else if (isset($_GET['acao']) && $_GET['acao'] == "sair") {
    if (isset($_COOKIE['logar'])) {
        if ($proce->logout()) {
            header("Location: index.php");
        }
    }
} else if ($_GET['acao'] == "mudarSenha" && isset($_GET['acao'])) {
    if (isset($_COOKIE['logar'])) {
        $senha = isset($_POST['pass']) ? addslashes($_POST['pass']) : null;
        $senha2 = isset($_POST['repass']) ? addslashes($_POST['repass']) : null;
        if ($senha != $senha2) {
            session_start();
            $_SESSION['erro'] = "Senhas diferentes!";
            header("Location: alterar.php");
            exit();
        }
        if ($senha == "" || $senha2 == "") {
            session_start();
            $_SESSION['erro'] = "Preencha todos os campos!";
            header("Location:   .php");
            exit();
        }
        if ($proce->mudarSenha($senha)) {
            session_start();
            $_SESSION['semErro'] = "Senha cadastrada com sucesso!";
            header("Location: index.php");
            exit();
        } else {
            session_start();
            $_SESSION['erro'] = "Senha já cadastrada!";
            header("Location: alterar.php");
            exit();

        }
    }
} else {
    header("Location: index.php");
}

?>
<?php
if (isset($_COOKIE['logar']) && $_COOKIE['logar'] == "logado") {
    session_start();
    echo "
        <!DOCTYPE html>
        <html lang=\"pt-BR\">
        <head>
            <meta charset=\"UTF-8\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
            <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
            <link rel=\"stylesheet\" href=\"css/uikit.min.css\" />
            <link rel=\"stylesheet\" href=\"css/meuCss.css\" />
            <title>Cadastro de usuario</title>
        </head>
        <body>
        <div class=\"uk-navbar-container\" uk-navbar>
            <div class=\"uk-navbar\">   
                <ul class=\"uk-navbar-nav\">
                    <li class=\"uk-active\"><a href=\"alterar.php\"><span class=\"uk-icon\" uk-icon=\"icon: pencil; ratio: 1.5;\"></span>mudar Senha</a></li>
                    <li><a href=\"processa.php?acao=sair\"><span class=\"uk-icon\" uk-icon=\"icon: sign-out; ratio: 1.5;\"></span>sair</a></li>
                </ul>
            </div>
            </div>
            <div id=\"form\">
                <p id=\"paraForm\" class=\"uk-text-center\">Alterar senha</p>
                <p id=\"erro\"></p>
                <form action=\"processo.php?acao=mudarSenha\" method=\"post\">
                    <div class=\"uk-inline uk-form-width-large\">
                        <a href=\"#\" class=\"uk-form-icon\" id=\"passO\" uk-icon=\"icon: lock; ratio: 1.5;\"></a>
                        <input type=\"password\" id=\"pass\" name=\"pass\" class=\"uk-input\" placeholder=\"Digite sua nova senha\" /> 
                    </div>
                    <br /><br />
                    <div class=\"uk-inline uk-form-width-large\">
                        <a id=\"passO2\" class=\"uk-form-icon\" uk-icon=\"icon: lock; ratio: 1.5;\"></a>
                        <input type=\"password\" id=\"repass\" name=\"repass\" onkeyup=\"senha(this.value);\" class=\"uk-input\" placeholder=\"Digite novamente a sua senha\" />
                    </div>
                    <p style=\"font-style: italic; margin-top: 0;\">*click no cadeado para ver a senha</p>
                    <input type=\"submit\" onclick=\"return validarNova();\" value=\"Mudar\" class=\"uk-button  uk-button-primary\"/>
                    <a href=\"index.php\" class=\"uk-link-reset uk-button uk-button-default\">Voltar</a>
                </form>
            </div>
        </body>
        <script src=\"js/uikit.min.js\"></script>
        <script src=\"js/uikit-icons.min.js\"></script>
        <script src=\"js/meuJs.js\"></script>";
    if (isset($_SESSION['erro'])) {
        echo "
                <script>
                    document.getElementById(\"erro\").classList = \"uk-alert uk-alert-danger\";
                    document.getElementById(\"erro\").innerHTML = \"{$_SESSION['erro']}\";
                </script>";
        unset($_SESSION['erro']);
    }
    echo "
        </html>
        ";
} else {
    header("Location: index.php");
}
?>


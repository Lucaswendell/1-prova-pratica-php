<?php
if (isset($_COOKIE['logar']) && $_COOKIE['logar'] == "logado") {
    session_start();
    echo "
    <!DOCTYPE html>
    <html lang=\"pt-BR\">
    <head>
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
        <link rel=\"stylesheet\" href=\"css/uikit.min.css\" />
        <link rel=\"stylesheet\" href=\"css/meuCss.css\" />
        <title>Cadastro de usuario</title>
    </head>
    <body>
    <div class=\"uk-navbar-container\" uk-navbar>
            <div class=\"uk-navbar\">   
                <ul class=\"uk-navbar-nav\">
                    <li><a href=\"alterar.php\"><span class=\"uk-icon\" uk-icon=\"icon: pencil; ratio: 1.5;\"></span>mudar Senha</a></li>
                    <li><a href=\"processo.php?acao=sair\"><span class=\"uk-icon\" uk-icon=\"icon: sign-out; ratio: 1.5;\"></span>sair</a></li>
                </ul>
            </div>
        </div>
    <h3 class=\"uk-text-center\">Olá {$_SESSION['user']}! Seja bem vindo a minha aplicação.</h3>
    </body>
    <script src=\"js/uikit.min.js\"></script>
    <script src=\"js/uikit-icons.min.js\"></script>
    <script src=\"js/meuJs.js\"></script>";
    if(isset($_SESSION['semErro'])){
		echo "
		<script>
			UIkit.notification({
				message: \"{$_SESSION['semErro']}\",
                status: \"success\",
                timeout: 3000
			});
        </script>";
        unset($_SESSION['semErro']);
	}
    echo "</html>";
    
} else {
    header("Location: index.php");
}

?>
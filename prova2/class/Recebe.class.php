<?php
/**
 * Essa classse vai fazer o cadastro do usuário, o login e muda a senha do usuário.
 * @author Rafael 
 */
class Recebe
{
    /**
     * Vai fazer o cadastro o usuário
     * @param string $nome Vai receber o nome do usuário.
     * @param string $senha Vai receber o nome do usuário.
     * @return boolean
     */
    public function cadastro($nome, $senha)
    { //faz o cadastro do usuario
        if (!file_exists("user/$nome")) {
            mkdir("user/$nome", 0777, true); //cria a pasta
            $arq = fopen("user/$nome/$nome.txt", "a"); //cria o arquivo senha
            fwrite($arq, "$senha%"); //adiciona no arquivo
            fclose($arq);
            session_start();
            setcookie("logar", "logado");
            $_SESSION['user'] = $nome;
            return true;
        } else {
            return false;
        }
    }
    /**
     *Vai fazer o login do usuário.
     * @param string $nome Pega o nome do usuário.
     * @param string $senha Pega a senha do usuário.
     * @return boolean
     */
    public function login($nome, $senha)
    {//faz o login do usuario
        $arq = fopen("user/$nome/$nome.txt", "r"); //coloca o arquivo de senha em um ponteiro
        $senhas = fread($arq, filesize("user/$nome/$nome.txt"));//ler o arquivo de senha
        $senhas = explode("%", $senhas); //transforma a senhas em array
        array_pop($senhas);
        $ultimo = $senhas[count($senhas) - 1]; //pega o ultimo índice
        if ($senha == $ultimo) { //verifica senha
            session_start();
            $_SESSION['user'] = $nome;
            setcookie("logar", "logado");
            return true;
        } else {
            return false;
        }

    }
    /**
     * Vai fazer o logout do usuário.
     * @return void
     */
    public function logout()
    {//faz o logout do usuario
        if (isset($_COOKIE['logar']) && $_COOKIE['logar'] == "logado") {
            setcookie("logar", null);
            setcookie("PHPSESSID", null);
            session_destroy();
            return true;
        }
    }
    /**
     * Vai mudar a senha do usuário.
     * @param string $senha Pega a nova do usuário.
     * @return boolean
     */
    public function mudarSenha($senha)
    {//muda a senha do usuario
        session_start();
        if (isset($_COOKIE['logar'])) {
            $tem = 0;
            $arq = fopen("user/{$_SESSION['user']}/{$_SESSION['user']}.txt", "r");
            $senhas = fread($arq, filesize("user/{$_SESSION['user']}/{$_SESSION['user']}.txt"));
            $senhas = explode("%", $senhas);
            array_pop($senhas);
            if (count($senhas) < 3) { //verifica se as senhas cadastradas são menores que 3 
                foreach ($senhas as $sen) {
                    if ($sen == $senha) { //verifica as senhas com a nova
                        $tem++;
                        break;
                    }
                }
                fclose($arq);
                if ($tem == 0) {
                    $arq = fopen("user/{$_SESSION['user']}/{$_SESSION['user']}.txt", "a");
                    fwrite($arq, "$senha%");
                    return true;
                } else {
                    return false;
                }
            } else {
                foreach ($senhas as $sen) {
                    if ($sen == $senha) { //verifica as senhas com a nova
                        $tem++;
                        break;
                    }
                }
                if ($tem == 0) {
                    $arq = fopen("user/{$_SESSION['user']}/{$_SESSION['user']}.txt", "w");
                    array_shift($senhas);
                    array_push($senhas, "$senha");
                    foreach($senhas as $sen){
                        fwrite($arq, "$sen%");
                    }
                    fclose($arq);
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
}

?>
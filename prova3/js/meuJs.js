//validação login
function validarLogin(){
    var user = document.getElementById("user").value;
    var pass = document.getElementById("pass").value;
    var erro = 0;
    if (user == "") {
        erro++;
    }
    if (pass == "") {
        erro++;
    }
    if (erro > 0) {
        document.getElementById("erro").classList = "uk-alert uk-alert-danger";
        document.getElementById("erro").innerHTML = "Preencha todos os campos!!!";
        return false;
    } else {
        return true;
    }
}
//validação cadastro
function validar() {
    var user = document.getElementById("user").value;
    var pass = document.getElementById("pass").value;
    var repass =  document.getElementById("repass").value;
    var erro = 0;
    if (user == "") {
        erro++;
    }
    if (pass == "" || repass == "") {
        erro++;
    }
    if (erro > 0) {
        document.getElementById("erro").classList = "uk-alert uk-alert-danger";
        document.getElementById("erro").innerHTML = "Preencha todos os campos!!!";
        return false;
    } else {
        return true;
    }
}
//validação mudar senha
function validarNova() {
    var pass = document.getElementById("pass").value;
    var repass = document.getElementById("repass").value;
    var erro = 0;
    if (pass == "" || repass == "") {
        erro++;
    }
    if (erro > 0) {
        document.getElementById("erro").classList = "uk-alert uk-alert-danger";
        document.getElementById("erro").innerHTML = "Preencha todos os campos!!!";
        return false;
    } else {
        return true;
    }
}
//validação senhas iguais
function senha(senha1) {
    var pass = document.getElementById("pass").value;
    if (pass != senha1) {
        document.getElementById("erro").classList = "uk-alert uk-alert-danger";
        document.getElementById("erro").innerHTML = "Senhas diferentes";
        return false;
    } else {
        document.getElementById("erro").classList = "";
        document.getElementById("erro").innerHTML = "";
        return true;
    }
}
//mostra senha
document.getElementById("passO").onclick = function(){
    if(document.getElementById("pass").type == "password"){
        document.getElementById("pass").type = "text";
        document.getElementById("passO").setAttribute("uk-icon","icon: unlock; ratio: 1.5;");
    }else{
        document.getElementById("pass").type = "password";
        document.getElementById("passO").setAttribute("uk-icon","icon: lock; ratio: 1.5;");
    }
};
//mostra senha
document.getElementById("passO2").onclick = function(){
    if(document.getElementById("repass").type == "password"){
        document.getElementById("repass").type = "text";
        document.getElementById("passO2").setAttribute("uk-icon","icon: unlock; ratio: 1.5;");
    }else{
        document.getElementById("repass").type = "password";
        document.getElementById("passO2").setAttribute("uk-icon","icon: lock; ratio: 1.5;");
    }
};
//mostra imagem selecionada
function aparece(img){
    imgSlect = img.split("\\");
    document.getElementById("imgSelect").innerHTML = "<span class=\"uk-icon\" uk-icon=\"icon:image; ratio:1.5;\"></span> "+imgSlect[2];
}
<?php
session_start();
if (isset($_COOKIE['logar']) && $_COOKIE['logar'] == "logado") {
    $img = false;
    if (file_exists("user/{$_SESSION['user']}/img")) {
        require_once("class/Processo.class.php");
        $img = opendir("user/{$_SESSION['user']}/img"); //coloca o diretorio img em um ponteiro
    }
    if ($img) {
        echo "
<!DOCTYPE html>
<html lang=\"pt-BR\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\" />
    <link rel=\"stylesheet\" href=\"css/uikit.min.css\" />
    <link rel=\"stylesheet\" href=\"css/meuCss.css\" />
    <title>Slider</title>
</head>
<body>
    <div class=\"uk-navbar-container\" uk-navbar>
            <div class=\"uk-navbar\">   
                <ul class=\"uk-navbar-nav\">
                    <li><a href=\"#\" uk-tooltip=\"title: usuário;\"><span class=\"uk-icon\" uk-icon=\"icon: user; ratio:1.5;\"></span>{$_SESSION['user']}</a></li>
                    <li><a href=\"alterar.php\"><span class=\"uk-icon\" uk-icon=\"icon: pencil; ratio: 1.5;\"></span>mudar Senha</a></li>
                    <li><a href=\"upload.php\"><span class=\"uk-icon\" id=\"dife\" uk-icon=\"icon: image;ratio:1.5;\"></span>Adicionar mais imagem</a></li>
                    <li><a href=\"processa.php?acao=sair\"><span class=\"uk-icon\" uk-icon=\"icon: sign-out; ratio: 1.5;\"></span>sair</a></li>
                </ul>
            </div>
            </div>
</div>
    <div class=\"slie uk-margin\">
        <div uk-slideshow=\"autoplay:true; animation:scale;autoplay-interval: 2000;\" id=\"slide\">
            <ul class=\"uk-slideshow-items\">
            ";
        $processo = new Processo();
        $processo->geraImg($img);
        echo "
            </ul>
            <ul class=\"uk-slideshow-nav uk-dotnav uk-flex-center uk-margin\"></ul>
        </div>
    </div>
</body>
<script src=\"js/uikit.min.js\"></script>
<script src=\"js/uikit-icons.min.js\"></script>
</html>";
    } else {
        $_SESSION['erro'] = "Você não tem imagem para ser mostrada no slider.";
        header("Location: index.php");
        exit();
    }
} else {
    header("Location: index.php");
}
?>
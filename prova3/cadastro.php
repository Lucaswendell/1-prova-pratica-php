<?php
    echo "
    <!DOCTYPE html>
    <html lang=\"pt-BR\">
    <head>
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
        <link rel=\"stylesheet\" href=\"css/uikit.min.css\" />
        <link rel=\"stylesheet\" href=\"css/meuCss.css\" />
        <title>Cadastro de usuario</title>
    </head>
    <body>
        <div id=\"form\">
            <p id=\"formP\" class=\"uk-text-center\">Cadastro de usuário</p>
            <p id=\"erro\"></p>
            <form action=\"processa.php?acao=cadastro\" method=\"post\">
                <div class=\"uk-inline uk-form-width-large\">
                    <span class=\"uk-form-icon\" uk-icon=\"icon: user; ratio: 1.5;\"></span>
                    <input type=\"text\" id=\"user\" name=\"nome\" class=\"uk-input\" placeholder=\"Digite seu nome \" />
                </div>
                <br /><br />
                <div class=\"uk-inline uk-form-width-large\">
                    <a href=\"#\" class=\"uk-form-icon\" id=\"passO\" uk-icon=\"icon: lock; ratio: 1.5;\"></a>
                    <input type=\"password\" id=\"pass\" name=\"pass\" class=\"uk-input\" placeholder=\"Digite sua senha\" /> 
                </div>
                <br /><br />
                <div class=\"uk-inline uk-form-width-large\">
                    <a id=\"passO2\" class=\"uk-form-icon\" uk-icon=\"icon: lock; ratio: 1.5;\"></a>
                    <input type=\"password\" id=\"repass\" name=\"repass\" onkeyup=\"senha(this.value);\" class=\"uk-input\" placeholder=\"Digite novamente a sua senha\" />
                </div>
                <p style=\"font-style: italic; margin-top: 0;\">*click no cadeado para ver a senha</p>
                <input type=\"submit\" onclick=\"return validar();\" value=\"Cadastrar\" class=\"uk-button uk-button-primary\"/>
                <a href=\"index.php\" class=\"uk-button uk-button-default\">Cancelar</a>

            </form>
        </div>
    </body>
    <script src=\"js/uikit.min.js\"></script>
    <script src=\"js/uikit-icons.min.js\"></script>
    <script src=\"js/meuJs.js\"></script>";
    session_start();
    if(isset($_SESSION['erro'])){
        echo "<script>
        document.getElementById(\"erro\").classList=\"uk-alert uk-alert-danger\";
        document.getElementById(\"erro\").innerHTML = \"{$_SESSION['erro']}\";
        </script>";
        unset($_SESSION['erro']);
    }
    echo "
    </html>
";
?>
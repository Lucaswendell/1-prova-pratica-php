<!DOCTYPE html>
<html>
<head>
<title>Login</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="css/uikit.min.css">
<link rel="stylesheet" type="text/css" href="css/meuCss.css">
</head>
<body>
	<div id="form">
	 <p id="formP" class="uk-text-center"> Login </p> 
	 <p id="erro"></p>
		<form action="processa.php?acao=logar" method="POST">
			<div class="uk-inline uk-form-width-large"> 
			<span class="uk-form-icon" uk-icon="icon:user; ratio: 1.5;" ></span>
			 <input type="text" id="user" name="nome" class="uk-input" placeholder="Digite seu usuário" >  </div> <br/>
			<br/>
			<div class="uk-inline uk-form-width-large"> 
			<a href="#" id="passO" class="uk-form-icon " uk-icon="icon:lock; ratio: 1.5;" ></a>
			<input type="password" id="pass" name="pass" class="uk-input" placeholder="Digite sua senha"> 
			</div>
			<p style="font-style: italic; margin-top: 0;">*click no cadeado para ver a senha</p>
			<input type="submit" value="Logar" class="uk-button uk-button-primary uk-align-center" onclick="return validarLogin();"> 
			<p>Acessando pela primeira vez? Cadastre-se para ter acesso ao nosso sistema.</p> 
		    <a href="cadastro.php" class="uk-button uk-button-primary uk-align-center uk-width-small"> Cadastrar </a>
		</form>
		</div>
<script src="js/uikit.min.js"></script>
<script src="js/uikit-icons.min.js"></script>
<script src="js/meuJs.js"></script>
<?php
	session_start();
	if(isset($_SESSION['erro'])){
		echo "<script>
		document.getElementById(\"erro\").classList=\"uk-alert uk-alert-danger\";
		document.getElementById(\"erro\").innerHTML = \"{$_SESSION['erro']}\";
		</script>";
		unset($_SESSION['erro']);
	}
?>
</body>
</html>

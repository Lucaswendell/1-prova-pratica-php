<?php
require_once("class/Processo.class.php");
$processo = new Processo();
session_start();    
if (isset($_GET['acao']) && $_GET['acao'] == "cadastro") { //cadastro do usuario
    $nome = (isset($_POST['nome'])) ? addslashes($_POST['nome']) : null; //pega o nome
    $senha = (isset($_POST['pass'])) ? addslashes($_POST['pass']) : null; //pega a senha
    $senha2 = (isset($_POST['pass'])) ? addslashes($_POST['repass']) : null; //pega a senha 2
    if ($senha != $senha2) { //verifica se a senha 1 é igual a senha 2
        $_SESSION['erro'] = "Senhas diferentes!";
        header("Location: cadastro.php");
        exit();
    }
    if ($senha == "" || $senha2 == "") { //verifica se os campos senhas estao vazios
        $_SESSION['erro'] = "Preencha todos os campos!";
        header("Location: cadastro.php");
        exit();
    }
    if ($nome != "") { //cria o usuario
        if ($processo->cadastro($nome, $senha)) {
            $_SESSION['semErro'] = "Usuário cadastrado com sucesso!";
            header("Location: index.php");
            exit();
        } else {
            $_SESSION['erro'] = "Usuário já cadastrado!";
            header("Location: cadastro.php");
            exit();
        }
    } else {
        $_SESSION['erro'] = "Preencha todos os campos!";
        header("Location: cadastro.php");
        exit();
    }
} else if (isset($_GET['acao']) && $_GET['acao'] == "sair") { //logout
    if ($processo->logOut("logar")) {
        header("Location: index.php");
    } else {
        header("Location: index.php");
        exit();
    }
} else if (isset($_GET['acao']) && $_GET['acao'] == "mudarSenha") {//mudar senha
    if (isset($_COOKIE['logar']) && $_COOKIE['logar'] == "logado") {
        $senha = (isset($_POST['pass'])) ? addslashes($_POST['pass']) : null;
        $senha2 = (isset($_POST['repass'])) ? addslashes($_POST['repass']) : null;
        if ($senha != $senha2) {
            $_SESSION['erro'] = "Senhas diferentes!";
            header("Location: alterar.php");
            exit();
        }
        if ($senha == "" || $senha2 == "") {
            $_SESSION['erro'] = "Preencha todos os campos!";
            header("Location: alterar.php");
            exit();
        }
        if (isset($_SESSION['user'])) {
            if ($processo->mudarSenha($senha)) {
                $_SESSION['semErro'] = "Senha alterada com sucesso!";
                header("Location: index.php");
                exit();
            } else {
                $_SESSION['erro'] = "Senha já cadastrada!";
                header("Location: alterar.php");
                exit();
            }
        }
    } else {
        header("Location: index.php");
    }
} else if (isset($_GET['acao']) && $_GET['acao'] == "imagem") { //enviar imagem
    if (isset($_COOKIE['logar']) && $_COOKIE['logar'] == "logado") {
        if ($processo->verificarImg($_FILES['foto']['type'])) {
            $ext = substr($_FILES['foto']['type'], 6);
            if (!file_exists("user/{$_SESSION['user']}/img")) {
                mkdir("user/{$_SESSION['user']}/img", 0777, true);
            }
            if ($processo->imagem($_FILES['foto']['tmp_name'], $ext)) {
                $_SESSION['semErro'] = "Imagem enviada com sucesso!";
                header("Location: index.php");
                exit();
            } else {
                $_SESSION['erro'] = "Erro ao enviar a imagem!";
                header("Location: index.php");
                exit();
            }
        } else {
            $_SESSION['erro'] = "Envie uma imagem valida (jpeg, png, jpg)!";
            header("Location: index.php");
            exit();
        }
    }
} else if (isset($_GET['acao']) && $_GET['acao'] == "logar") {
    $nome = (isset($_POST['nome'])) ? addslashes($_POST['nome']) : null;
    $senha = (isset($_POST['pass'])) ? addslashes($_POST['pass']) : null;
    if (file_exists("user/$nome")) {
        if ($processo->logar($nome, $senha)) {
            header("Location: index.php");
        } else {    
            $_SESSION['erro'] = "senha incorreta!";
            header("Location: index.php");
            exit();
        }
    } else {
        $_SESSION['erro'] = "Usuário não cadastrado!";
        header("Location: index.php");
        exit();
    }
} else {
    header("Location: index.php");
}
?>
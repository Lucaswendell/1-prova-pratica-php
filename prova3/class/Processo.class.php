<?php

/**
 * essa classe vai enviar imagem, verificar a imagem, cadastrar o usuário, fazer o logout, fazer o login
 * @author Eduardo
 * @var array $mensagens recebe a legenda/mensagem do usuário
 * 
 */
class Processo
{
    /**
     * recebe a legenda da imagem
     * @var array 
     */
    public $mensagens = array();

    /**
     * vai verificar se o arquivo é imagem
     *@param array $img vai receber o arquivo
     *@return bool 
     */
    public function verificarImg($img)
    { //verificar se o arquivo é imagem
        $ext = "";
        switch ($img) { //verifica a extensão
            case "image/png":
                $ext = true;
                break;
            case "image/jpeg":
            case "image/jpg":
                $ext = true;
                break;
            default:
                $ext = false;
        }
        return $ext;
    }
    /**
     * vai criar o usuário
     * @param string $nome vai receber o nome do usuário
     * @param string $senha vai receber a senha
     * @return bool
     */
    public function cadastro($nome, $senha)
    { //cria o usuario
        if (!file_exists("user/$nome")) {
            mkdir("user/$nome", 0777, true);
            $arqu = fopen("user/$nome/$nome.txt", "a");
            fwrite($arqu, "$senha%");
            fclose($arqu);
            setcookie("logar", "logado");
            $_SESSION["user"] = $nome;
            return true;
        } else {
            return false;
        }
    }
    /**
     * vai fazer o logout
     * @param string $nomeCookie vai receber o nome do cookie
     * @return bool
     */
    public function logOut($nomeCookie)
    { //faz o logout
        if (isset($_COOKIE[$nomeCookie])) {
            setcookie($nomeCookie, null);
            setcookie("PHPSESSID",null);
            session_destroy();
            return true;
        } else {
            return false;
        }
    }
    /**
     * vai mudar a senha do usuário
     * @param string $senha vai receber a senha nova
     * @return bool
     */
    public function mudarSenha($senha) //mudar a senha do usuario
    {
        $arqSenh = fopen("user/{$_SESSION['user']}/{$_SESSION['user']}.txt", "r");
        $Lsenhas = fread($arqSenh, filesize("user/{$_SESSION['user']}/{$_SESSION['user']}.txt"));
        $senhas = explode("%", $Lsenhas);
        array_pop($senhas);
        if (count($senhas) == 3) {
            $erro = 0;
            foreach ($senhas as $sen) {
                if ($sen == $senha) {
                    $erro++;
                    break;
                }
            }
            if ($erro == 0) {
                array_shift($senhas);
                array_push($senhas, $senha);
                $arqu = fopen("user/{$_SESSION['user']}/{$_SESSION['user']}.txt", "w");
                foreach ($senhas as $sen) {
                    fwrite($arqu, "$sen%");
                }
                fclose($arqu);
                return true;
            } else {
                return true;
            }
        } else {
            $erro = 0;
            foreach ($senhas as $sena) {
                if ($sena == $senha) {
                    return false;
                    $erro++;
                    break;
                }
            }
            if ($erro == 0) {
                $arqu = fopen("user/{$_SESSION['user']}/{$_SESSION['user']}.txt", "a");
                fwrite($arqu, "$senha%");
                fclose($arqu);
                return true;
            }
        }

    }
    /**
     * vai enviar a imagem
     * @param array $img vai receber a imagem do usuário
     * @param string $ext vai receber a extensão da imagem
     * @return bool
     */
    public function imagem($img, $ext)
    { //envia a imagem
        $certo = 0;
        if (!file_exists("user/{$_SESSION['user']}/img/1foto$ext.$ext")) {
            move_uploaded_file($img, "user/{$_SESSION['user']}/img/1foto$ext.$ext");//envia a imagem como se fosse a primeira
            if ($_POST['men']) { //gera um indice que armazena a descrisão do usuario
                if (!file_exists("user/{$_SESSION['user']}/img/men")) { //cria a pasta men caso ela nao exista
                    mkdir("user/{$_SESSION['user']}/img/men", 0777, true);
                }
                $arq = fopen("user/{$_SESSION['user']}/img/men/1foto$ext.txt", "a");//cria o arquivo de texto com a mensagem
                fwrite($arq, $_POST['men']);
                fclose($arq);
            }
            $certo++;
        } else {
            $n = 0;
            $novo = "";
            while (file_exists("user/{$_SESSION['user']}/img/1foto$ext.$ext")) {//cria um novo nome caso o arquivo exista
                $novo = "{$n}foto$ext.$ext";
                if (file_exists("user/{$_SESSION['user']}/img/$novo")) {
                    $n++;
                } else {
                    break;
                }
            }
            move_uploaded_file($img, "user/{$_SESSION['user']}/img/$novo");//envia a imagem com um novo nome
            if ($_POST['men']) { //gera um indice que armazena a descrisão do usuario
                if (!file_exists("user/{$_SESSION['user']}/img/men")) { //cria a pasta caso ela nao exista
                    mkdir("user/{$_SESSION['user']}/img/men", 0777, true);
                }
                $nomeArqu = "{$n}foto$ext.$ext";
                $arq = fopen("user/{$_SESSION['user']}/img/men/{$n}foto$ext.txt", "a");
                fwrite($arq, $_POST['men']);
                fclose($arq);
            }
            $certo++;
        }
        if ($certo > 0) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * vai logar o usuário
     * @param string $nome vai rceber o nome do usuário
     * @param string $senha vai receber a senha do usuário
     * @return bool
     */
    public function logar($nome, $senha)
    {
        $arqSenh = fopen("user/{$_POST['nome']}/{$_POST['nome']}.txt", "r");
        $Lsenhas = fread($arqSenh, filesize("user/{$_POST['nome']}/{$_POST['nome']}.txt"));
        $senhas = explode("%", $Lsenhas);
        array_pop($senhas);
        $ultimo = count($senhas) - 1;
        if ($senha == $senhas[$ultimo]) {
            if (!isset($_COOKIE['PHPSESSID'])) {
                session_start();
            }
            $_SESSION['user'] = $nome;
            setcookie("logar", "logado");
            return true;
        } else {
            return false;
        }
    }
    /**
     * vai gerar a imagem
     * @param string $img vai ser o ponteiro do diretório imagem
     * @return void
     */
    public function geraImg($img)
    {//gera as li's
        if (file_exists("user/{$_SESSION['user']}/img/men")) {
            $men = opendir("user/{$_SESSION['user']}/img/men");
            while (false != ($mena = readdir($men))) { //ler o diretório mensagem
                if ($mena == "." || $mena == "..") {
                    continue;
                } else {
                    $im = substr($mena, 0, 8);//pega a metade do nome
                    $arqu = fopen("user/{$_SESSION['user']}/img/men/$mena", "r");
                    $mensagens[$im]["men"] = fread($arqu, filesize("user/{$_SESSION['user']}/img/men/$mena")); //adiciona um indice com nome da imagem e a mensagem
                }
            }
        }
        while (false != ($ima = readdir($img))) {//ler o diretoria imagem
            if ($ima == "." || $ima == ".." || $ima == "men") {
                continue;
            } else {
                $im = substr($ima, 0, 8);
                echo "
                <li>
                    <img src=\"user/{$_SESSION['user']}/img/$ima\">";
                if (isset($mensagens[$im]["men"])) { //verifica se existe legenda sobre a imagem
                    echo "
                    <div class=\"uk-overlay uk-overlay-primary uk-position-bottom uk-text-center uk-animation-slide-bottom\">
                        <p class=\"uk-margin-remove\">{$mensagens[$im]["men"]}</p>
                    </div>
            ";
                }
                echo "
        </li>";
            }
        }
    }
}
?>
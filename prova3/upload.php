<?php
if (isset($_COOKIE['logar']) && $_COOKIE['logar'] == "logado") {
    session_start();
    echo "
        <!DOCTYPE html>
        <html lang=\"pt-BR\">
        <head>
            <meta charset=\"UTF-8\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
            <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
            <link rel=\"stylesheet\" href=\"css/uikit.min.css\" />
            <link rel=\"stylesheet\" href=\"css/meuCss.css\">
            <title>Envie sua imagem</title>
        </head>
        <body>
            <div class=\"uk-navbar-container\" uk-navbar>
            <div class=\"uk-navbar\">   
                <ul class=\"uk-navbar-nav\">
                    <li class=\"uk-active\"><a href=\"#\" uk-tooltip=\"title: usuário;\"><span class=\"uk-icon\" uk-icon=\"icon: user; ratio:1.5;\"></span>{$_SESSION['user']}</a></li>
                    <li><a href=\"alterar.php\"><span class=\"uk-icon\" uk-icon=\"icon: pencil; ratio: 1.5;\"></span>mudar Senha</a></li>
                    <li><a href=\"slide.php\"><span class=\"uk-icon\" id=\"dife\" uk-icon=\"icon: album;ratio:1.5;\"></span>Slider</a></li>
                    <li><a href=\"processa.php?acao=sair\"><span class=\"uk-icon\" uk-icon=\"icon: sign-out; ratio: 1.5;\"></span>sair</a></li>
                </ul>
            </div>
            </div>
            <div id=\"form\">
                <p id=\"formP\" class=\"uk-text-center\">Envie uma imagem e veja ela passar em um slider</p>
                <form action=\"processa.php?acao=imagem\" method=\"post\" enctype=\"multipart/form-data\">
                    <textarea placeholder=\"Digite uma legenda para a imagem(opcional)\" class=\"uk-textarea\" name=\"men\" id=\"legenda\"
                    cols=\"20\" rows=\"10\" onblur=\"limite(this.value)\"></textarea><br /><br />
                    <div class=\"uk-placeholder uk-text-center\">
                    <p id=\"imgSelect\"><span class=\"uk-icon\" uk-icon=\"icon:image; ratio:1.5;\"></span> Nenhuma imagem selecionada</p>  
                    <div uk-form-custom>
                    <span uk-icon=\"icon: cloud-upload\"></span>
                        <input type=\"file\" name=\"foto\" onchange=\"aparece(this.value)\" id=\"file\" accept=\"image/jpeg,image/png,image/jpg\"/>
                        <span class=\"uk-link\">Selecione a imagem</span>
                    </div>
                </div><br /><br />
                    <input type=\"submit\" value=\"enviar\" class=\"uk-button uk-button-primary\">
                </form>
            </div>
        </body>
        <script src=\"js/uikit.min.js\"></script>
        <script src=\"js/uikit-icons.min.js\"></script>
        <script src=\"js/meuJs.js\"></script>";
    if (isset($_SESSION['semErro'])) {
        echo "
                    <script>
                        UIkit.notification({
                            message: \"{$_SESSION['semErro']}\",
                            status: \"success\"
                        })
                    </script>";
        unset($_SESSION['semErro']);
    } else if (isset($_SESSION['erro'])) {
        echo "
                    <script>
                        UIkit.notification({
                            message: \"{$_SESSION['erro']}\",
                            status: \"danger\"
                        })
                    </script>";
        unset($_SESSION['erro']);
    }else{
        unset($_SESSION['erro']);
        unset($_SESSION['semErro']);
    }  
    echo "
    </html>
        ";
} else {
    header("Location: index.php");
}
?>
